import { Component } from '@angular/core';

// Components
import {ECMenuComponent} from './components/ec-menu/ec-menu.component';
import {EcTopBarComponent} from './components/ec-top-bar/ec-top-bar.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    title = 'app';
}
