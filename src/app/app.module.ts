import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Components
import { AppComponent } from './app.component';
import { ECMenuComponent } from './components/ec-menu/ec-menu.component';
import { EcTopBarComponent } from './components/ec-top-bar/ec-top-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    ECMenuComponent,
    EcTopBarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
