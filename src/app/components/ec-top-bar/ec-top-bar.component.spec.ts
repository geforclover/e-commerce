import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcTopBarComponent } from './ec-top-bar.component';

describe('EcTopBarComponent', () => {
  let component: EcTopBarComponent;
  let fixture: ComponentFixture<EcTopBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcTopBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcTopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
