import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ECMenuComponent } from './ec-menu.component';

describe('ECMenuComponent', () => {
  let component: ECMenuComponent;
  let fixture: ComponentFixture<ECMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ECMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ECMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
